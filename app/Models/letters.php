<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class letters extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'date_in',
        'penerima',
        'content',
        'user_id',
        'type_id',
        'attachment_id'
    ];
    
}
