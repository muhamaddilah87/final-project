<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(){
        $user = User::with(['profile'])->get();
        return view('/pages/user/user-list', ['userList' => $user]);
    }
    public function show($id){

        $user = User::findOrFail($id);
        return view('/pages/user/user-detail',['userData' => $user]);
    }
    public function create(){
        $user = User::with(['profile'])->get();
        return view('/pages/user/user-create', ['userData' => $user]);
    }
    public function store(Request $request)
    {
        // dd($request->all());        
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'profile_id' => 'required',
        ]);
        User::create($request->all());
        return redirect('/user');
    }

    public function edit($id)
    {
        $user = User::with(['profile'])->findOrFail($id);
        $profile = Profile::select('id','name')->get();
        return view('/pages/user/user-edit', ['userData'=> $user, 'profileData' => $profile]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'profile_id' => 'required',
        ]);

        $user = User::findOrFail($id);
        $user->update($request->all());
        return redirect('/user');
    }
    public function destroy($id)
    {
        User::findOrFail($id)->delete();
        return redirect('/user');
    }
}
