<?php

namespace App\Http\Controllers;

use App\Models\attachment;
use App\Models\letters;
use App\Models\Type;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Backtrace\File;

use function PHPSTORM_META\type;

class SuratController extends Controller
{
    public function view(){
        $letter = letters::get()->all();
        return view('pages.surat.surat', ['letter' => $letter]);
    }
    public function create(){
        $type =Type::all();
        $user = User::all();

        return view('pages.surat.tambah', ['type' => $type], ['user' => $user]);
    }
    public function store(Request $request){
        $request->validate([
            'title' => 'required',
            'date_in' => 'required',
            'penerima' => 'required',
            'content' => 'required',
            'user_id' => 'required',
            'type_id' => 'required',
            'image' => 'required|mimes:png,jpg,jpeg|max:2048'
        ]);
        
        $imageName = time().'.'.$request->image->extension();

        $request->image->move(public_path('image'), $imageName);

        $surat = new letters;

        $surat->title = $request->title;
        $surat->date_in = $request->date_in;
        $surat->penerima = $request->penerima;
        $surat->content = $request->content;
        $surat->user_id = $request->user_id;
        $surat->type_id = $request->type_id;
        $surat->image = $imageName;

        $surat->save();

        return redirect('/surat');
    }

    public function show($id){
        $surat = letters::findOrFail($id);

        return view('pages.surat.detail',['surat'=>$surat]);
    }

    public function edit($id){
        $type =Type::all();
        $surat = letters::find($id);
        $user = User::all();

        return view('pages.surat.edit', ['surat' => $surat], ['type' => $type], ['user' => $user]);
    }

    public function update(Request $request, $id){
        $request->validate([
            'title' => 'required',
            'date_in' => 'required',
            'penerima' => 'required',
            'content' => 'required',
            'user_id' => 'required',
            'type_id' => 'required',
            'image' => 'mimes:png,lpg,jpeg|max:2048'
        ]);

        $surat = letters::find($id);
        
        $surat->title = $request->title;
        $surat->date_in = $request->date_in;
        $surat->penerima = $request->penerima;
        $surat->content = $request->content;
        $surat->user_id = $request->user_id;
        $surat->type_id = $request->type_id;

        if ($request->has('image')) {
            $path = 'image/';
            //File::delete($path. $file->file);

            $imageName = time().'.'.$request->image->extension();

            $request->image->move(public_path('image'), $imageName);
        }

        $surat->save();

        return redirect('/surat');
    }

    public function destroy($id){
        $surat = letters::find($id);

        if ($surat != null) {
            $surat->delete();
        }
        //$surat->delete();
        
        //$path = 'image/';
        //File::delete($path. $file->file);
        //$file->delete();

        return redirect('/surat');
    }

}
