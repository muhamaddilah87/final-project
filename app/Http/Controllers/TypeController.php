<?php

namespace App\Http\Controllers;
use App\Models\Type;

use Illuminate\Http\Request;

class TypeController extends Controller
{

    public function index(){
        $type = Type::get()->all();
        // dd($type);
        return view('/pages/type/type', ['typeList' => $type]);
    }
    public function show($id){

        $type = Type::findOrFail($id);
        return view('/pages/type/detail',['typeData' => $type]);
    }
    public function create(){
        return view('/pages/type/tambah');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'keterangan' => 'required',
        ]);
        Type::create($request->all());
        return redirect('/type');
    }

    public function edit($id)
    {
        $type = Type::findOrFail($id);
        return view('/pages/type/edit', ['typeData'=> $type]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'keterangan' => 'required',
        ]);

        $type = Type::findOrFail($id);
        $type->update($request->all());
        return redirect('/type');
    }
    public function destroy($id)
    {
        Type::findOrFail($id)->delete();
        return redirect('/type');
    }
}
