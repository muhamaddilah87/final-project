<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(){
        $profile = Profile::get()->all();
        return view('/pages/profile/profile-list', ['profileList' => $profile]);
    }
    public function show($id){

        $profile = Profile::findOrFail($id);
        return view('/pages/profile/profile-detail',['profileData' => $profile]);
    }
    public function create(){
        return view('/pages/profile/profile-create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'keterangan' => 'required',
        ]);
        Profile::create($request->all());
        return redirect('/profile');
    }

    public function edit($id)
    {
        $profile = Profile::findOrFail($id);
        return view('/pages/profile/profile-edit', ['profile'=> $profile]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'keterangan' => 'required',
        ]);

        $profile = Profile::findOrFail($id);
        $profile->update($request->all());
        return redirect('/profile');
    }
    public function destroy($id)
    {
        Profile::findOrFail($id)->delete();
        return redirect('/profile');
    }

}
