<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SuratController;
use App\Http\Controllers\TypeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages/dashboard');
});

// begin route of profile
Route::get('/profile',[ProfileController::class, 'index']);
Route::get('/profile/{id}',[ProfileController::class, 'show']);
Route::get('/profile-create',[ProfileController::class, 'create']);
Route::post('/profile',[ProfileController::class, 'store']);
Route::get('/profile/{id}/edit',[ProfileController::class, 'edit']);
Route::put('/profile/{id}',[ProfileController::class, 'update']);
Route::delete('/profile/{id}',[ProfileController::class, 'destroy']);
// end route of profile

// begin route of profile
Route::get('/user',[UserController::class, 'index']);
Route::get('/user/{id}',[UserController::class, 'show']);
Route::get('/user-create',[UserController::class, 'create']);
Route::post('/user',[UserController::class, 'store']);
Route::get('/user/{id}/edit',[UserController::class, 'edit']);
Route::put('/user/{id}',[UserController::class, 'update']);
Route::delete('/user/{id}',[UserController::class, 'destroy']);
// end route of profile


//route for type
Route::get('/type',[TypeController::class, 'index']);
Route::get('/type/{id}',[TypeController::class, 'show']);
Route::get('/type-create',[TypeController::class, 'create']);
Route::post('/type',[TypeController::class, 'store']);
Route::get('/type/{id}/edit',[TypeController::class, 'edit']);
Route::put('/type/{id}',[TypeController::class, 'update']);
Route::delete('/type/{id}',[TypeController::class, 'destroy']);
//end route of type

// Silahkan kerjakan yang ini
// begin route for letter
Route::get('/surat',[SuratController::class, 'view']);
Route::get('/surat/{id}',[SuratController::class, 'show']);
Route::get('/surat-create',[SuratController::class, 'create']);
Route::post('/surat',[SuratController::class, 'store']);
Route::get('/surat/{id}/edit',[SuratController::class, 'edit']);
Route::put('/surat{id}',[SuratController::class, 'update']);

Route::delete('/surat/{id}', [SuratController::class, 'destroy']);
//end route for letter
