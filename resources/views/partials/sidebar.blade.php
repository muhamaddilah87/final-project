<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
      <div class="sidebar-brand-text mx-3">Sistem Arsip</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="/">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>
      <hr class="sidebar-divider my-0">
    <li class="nav-item">
      <a class="nav-link" href="/surat">
        <i class="fas fa-solid fa-bars"></i>
        <span>Surat Masuk</span></a>
    </li>

    <!-- Divider -->
    <li class="nav-item">
      <a class="nav-link" href="/type">
        <i class="fas fa-fw fa-book"></i>
        <span>Jenis Surat</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/profile">
          <i class="fas fa-solid fa-user"></i>
          <span>Profile</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/user">
          <i class="fas fa-solid fa-users"></i>
          <span>User</span></a>
      </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

  </ul>