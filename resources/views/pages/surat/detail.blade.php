@extends('layouts.master')

@section('title')
    Arsip | Letter
@endsection

@section('judul-page')
    List Letter
@endsection

@section('content')
    <div>
         <!-- Page Section -->
         <div class="card p-4 mx-4 ">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                   
                    <tr>
                        <th>Nama</th>
                        <td>{{$surat->title}}</td>
                    </tr>
                    <tr>
                        <th>Tanggal Masuk</th>
                        <td>{{$surat->date_in}}</td>
                    </tr>
                    <tr>
                        <th>Penerima</th>
                        <td>{{$surat->penerima}}</td>
                    </tr>
                    <tr>
                        <th>Lampiran</th>
                        <td>{{$surat->content}}</td>
                    </tr>
                    <tr>
                        <th>User</th>
                        <td>{{$surat->user_id}}</td>
                    </tr>
                    <tr>
                        <th>Jenis Surat</th>
                        <td>{{$surat->type_id}}</td>
                    </tr>
                    <tr>
                        <th>File</th>
                        <td>{{$surat->attachment_id}}</td>
                    </tr>
                </thead>
            </table>
            <div>
                <a href="/surat" class="btn btn-success">Back</a>
            </div>        
         </div>
    </div>
@endsection