@extends('layouts.master')

@section('title')
    Arsip | include
@endsection

@section('judul-page')
    Tambah Data Surat
@endsection

@section('content')

<form class="px-4" action="/surat" method="POST">
    @csrf
    <div class="form-group">
        <label>Judul Surat Masuk</label>
        <input type="text" class="form-control" name="title" placeholder="Enter Title">
    </div>
    @error('title')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Tanggal Surat Masuk</label>
        <input type="date" class="form-control" name="date_in" placeholder="dd/mm/yyyy">
    </div>
    @error('date_in')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Penerima Surat</label>
        <input type="text" class="form-control" name="penerima" placeholder="Penerima Surat">
    </div>
    @error('penerima')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Lampiran Surat</label>
        <input type="text" class="form-control" name="content" placeholder="Lampiran Surat">
    </div>
    @error('content')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>user</label><br>
        <select class="form-control" name="user_id">
            <option>--.--</option>
            @foreach ($user as $user)
                <option value="{{$user->id}}">{{$user->name}}</option>
            @endforeach
        </select>
    </div>
    @error('user_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Jenis Surat</label><br>
        <select class="form-control" name="type_id">
            <option>--.--</option>
            @foreach ($type as $type)
                <option value="{{$type->id}}">{{$type->name}}</option>
            @endforeach
        </select>
    </div>
    @error('type_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Upload File</label>
        <input type="file" class="form-cotrol-file" name="image">
        <p class="text-danger">File berupa file scan (.png, .jpg, .jpeg)</p>
    </div>
    @error('image')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection