@extends('layouts.master')

@section('title')
    Arsip | include
@endsection

@section('judul-page')
    Tambah Data Surat
@endsection

@section('content')

<form class="px-4" action="/surat/{{$surat->id}}" method="post" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Judul Surat Masuk</label>
        <input type="text" class="form-control" name="title" placeholder="Enter Title" value="{{$surat->title}}">
    </div>
    @error('title')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Tanggal Surat Masuk</label>
        <input type="date" class="form-control" name="date_in" placeholder="dd/mm/yyyy" value="{{$surat->date_in}}">
    </div>
    @error('date_in')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Penerima Surat</label>
        <input type="text" class="form-control" name="penerima" placeholder="Penerima Surat" value="{{$surat->penerima}}">
    </div>
    @error('penerima')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Lampiran Surat</label>
        <input type="text" class="form-control" name="content" placeholder="Lampiran Surat" value="{{$surat->content}}">
    </div>
    @error('content')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>User</label><br>
        <select class="form-control" name="user_id">
            <option>--.--</option>
            @foreach ($user as $user)
                @if ($user->id === $surat->user_id)
                    <option value="{{$user->id}}" selected>{{$user->name}}</option>                    
                @else
                <option value="{{$user->id}}">{{$user->name}}</option>
                @endif
            @endforeach
        </select>
    </div>
    @error('user_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Jenis Surat</label><br>
        <select class="form-control" name="type_id">
            <option>--.--</option>
            @foreach ($type as $type)
                @if ($type->id === $surat->type_id)
                    <option value="{{$type->id}}" selected>{{$type->name}}</option>                    
                @else
                <option value="{{$type->id}}">{{$type->name}}</option>
                @endif
            @endforeach
        </select>
    </div>
    @error('type_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Upload File</label>
        <input type="file" class="form-cotrol-file" name="image">
        <p class="text-danger">File berupa file scan (.png, .jpg, .jpeg)</p>
    </div>
    @error('image')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <a href="/surat" class="btn btn-success">Back</a>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection