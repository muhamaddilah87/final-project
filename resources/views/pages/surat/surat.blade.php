@extends('layouts.master')

@section('title')
    Arsip | Surat
@endsection

@section('judul-page')
    Data Surat
@endsection

@section('content')
<div>
  <div class="card p-4 mx-4">
    <div class="d-flex align-items-center justify-content-between mb-4">
      <h5>Letter Information</h5>
      <a href="/surat-create" class="btn btn-success">Create Data</a>
    </div>
    <div class="table-responsive">
      <table class="table table table-bordered table-striped">
          <thead class="text-center">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nama Surat</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody class="text-center">
            @foreach ($letter as $key => $item)
                <tr>
                  <th scope="row">{{$key + 1}}</th>
                  <td>{{$item->title}}</td>
                  <td class="d-flex justify-content-center">
                    <a href="/surat/{{$item->id}}" class="btn btn-info mx-1">Detail</a>
                    <a href="/surat/{{$item->id}}/edit" class="btn btn-success mx-1">Edit</a>
                    <form action="/surat/{{$item->id}}" method="post" class="mx-1" style="display: inline-block">
                      @csrf
                      @method('delete')
                      <button class="btn btn-danger" value="delete">Delete</button>
                    </form>
                  </td>
                </tr>
            @endforeach
          </tbody>
  </table>
    </div>
  </div>
</div>
@endsection