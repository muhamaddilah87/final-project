@extends('layouts.master')

@section('title')
    Arsip | Type
@endsection

@section('judul-page')
    List Type
@endsection

@section('content')
    <div>
         <!-- Page Section -->
         <div class="card p-4 mx-4 ">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                   
                    <tr>
                        <th>Nama</th>
                        <td>{{$userData->name}}</td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td>{{$userData->email}}</td>
                    </tr>   
                    <tr>
                        <th>Profile</th>
                        <td>{{$userData->profile['name']}}</td>
                    </tr>                 
                </thead>
            </table>
            <div>
                <a href="/user" class="btn btn-success">Back</a>
            </div>        
         </div>
    </div>
@endsection