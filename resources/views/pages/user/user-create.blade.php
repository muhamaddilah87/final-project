@extends('layouts.master')

@section('title')
    Arsip | Input
@endsection

@section('judul-page')
    Tambah Data Jenis
@endsection

@section('content')

<form class="px-4" action="/user" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" name="name" placeholder="Enter Name">
    </div>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror   
    <div class="form-group">
        <label>Email</label>
        <input type="email" class="form-control" name="email" placeholder="email">
    </div>
    @error('email')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Password</label>
        <input type="password" class="form-control" name="password" placeholder="Password">
    </div>
    @error('password')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Profile</label>
        <select type="profile_id" class="form-control" name="profile_id">
            <option value="">Select...</option>
            @foreach ($userData as $item)
            <option value="{{$item->profile['id']}}">{{$item->profile['name']}}</option>
            @endforeach
        </select>
    </div>
    @error('profile_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <a href="/user" class="btn btn-success">Back</a>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection