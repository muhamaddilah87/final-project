@extends('layouts.master')

@section('title')
    Arsip | User
@endsection

@section('judul-page')
    Judul Halaman
@endsection

@section('content')
    <div>
         <!-- Page Section -->
         <div>
            <div class="card p-4 mx-4">
              <div class="d-flex align-items-center justify-content-between mb-4">
                <h5>Type Information</h5>
                <a href="/user-create" class="btn btn-success">Create Data</a>
              </div>
              <div class="table-responsive">
                <table class="table table table-bordered table-striped">
                    <thead class="text-center">
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Profile</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody class="text-center">
                      @foreach ($userList as $key => $item)
                          <tr>
                            <th scope="row">{{$key + 1}}</th>
                            <td>{{$item->name}}</td>
                            <td>{{$item->email}}</td>
                            <td>{{$item->profile['name']}}</td>
                            <td class="d-flex justify-content-center">
                              <a href="/user/{{$item->id}}" class="btn btn-info mx-1">Detail</a>
                              <a href="/user/{{$item->id}}/edit" class="btn btn-success mx-1">Edit</a>
                              <form action="/user/{{$item->id}}" method="post" class="mx-1" style="display: inline-block">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger" value="delete">Delete</button>
                              </form>
                            </td>
                          </tr>
                      @endforeach
                    </tbody>
            </table>
              </div>
            </div>
          </div>
    </div>
@endsection