@extends('layouts.master')

@section('title')
    Arsip | Input
@endsection

@section('judul-page')
    Tambah Data Jenis
@endsection

@section('content')

<form class="px-4" action="/user/{{$userData->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama</label>
        <input type="text" value="{{$userData->name}}" class="form-control" name="name" placeholder="Enter Name">
    </div>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror   
    <div class="form-group">
        <label>Email</label>
        <input type="email" value="{{$userData->email}}" class="form-control" name="email" placeholder="email">
    </div>
    @error('email')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Password</label>
        <input type="password" value="{{$userData->password}}" class="form-control" name="password" placeholder="Password">
    </div>
    @error('password')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Profile</label>
        <select type="profile_id" class="form-control" name="profile_id">
            <option value="{{$userData->profile['id']}}">{{$userData->profile['name']}}</option>
            @foreach ($profileData as $item)
            <option value="{{$item->id}}">{{$item->name}}</option>
            @endforeach
        </select>
    </div>
    @error('profile_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <a href="/user" class="btn btn-success">Back</a>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection