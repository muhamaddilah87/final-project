@extends('layouts.master')

@section('title')
    Arsip | Type
@endsection

@section('judul-page')
    Data Type
@endsection

@section('content')
<div>
  <div class="card p-4 mx-4">
    <div class="d-flex align-items-center justify-content-between mb-4">
      <h5>Type Information</h5>
      <a href="/type-create" class="btn btn-success">Create Data</a>
    </div>
    <div class="table-responsive">
      <table class="table table table-bordered table-striped">
          <thead class="text-center">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Jenis Surat</th>
              <th scope="col">Keterangan Surat</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody class="text-center">
            @foreach ($typeList as $key => $item)
                <tr>
                  <th scope="row">{{$key + 1}}</th>
                  <td>{{$item->name}}</td>
                  <td>{{$item->keterangan}}</td>
                  <td class="d-flex justify-content-center">
                    <a href="/type/{{$item->id}}" class="btn btn-info mx-1">Detail</a>
                    <a href="/type/{{$item->id}}/edit" class="btn btn-success mx-1">Edit</a>
                    <form action="/type/{{$item->id}}" method="post" class="mx-1" style="display: inline-block">
                      @csrf
                      @method('delete')
                      <button class="btn btn-danger" value="delete">Delete</button>
                    </form>
                  </td>
                </tr>
            @endforeach
          </tbody>
  </table>
    </div>
  </div>
</div>
@endsection