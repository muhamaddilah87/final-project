@extends('layouts.master')

@section('title')
    Arsip | Edit
@endsection

@section('judul-page')
    Edit Data Jenis
@endsection

@section('content')

<form class="px-4" action="/type/{{$typeData->id}}" method="post" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Jenis Surat Masuk</label>
        <input type="text" class="form-control" name="name" placeholder="Enter Title" value="{{$typeData->name}}">
    </div>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Keterangan</label>
        <input type="text" class="form-control" name="keterangan" placeholder="Keterangan" value="{{$typeData->keterangan}}">
    </div>
    @error('keterangan')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <a href="/surat" class="btn btn-success">Back</a>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection