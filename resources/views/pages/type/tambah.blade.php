@extends('layouts.master')

@section('title')
    Arsip | Input
@endsection

@section('judul-page')
    Tambah Data Jenis
@endsection

@section('content')

<form class="px-4" action="/type" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Jenis Surat Masuk</label>
        <input type="text" class="form-control" name="name" placeholder="Enter Name">
    </div>
    @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror   
    <div class="form-group">
        <label>Keterangan</label>
        <input type="text" class="form-control" name="keterangan" placeholder="Keterangan">
    </div>
    @error('keterangan')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <a href="/type" class="btn btn-success">Back</a>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection