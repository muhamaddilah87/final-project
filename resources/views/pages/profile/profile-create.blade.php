@extends('layouts.master')

@section('title')
    Arsip | Profile
@endsection

@section('judul-page')
    Create Profile
@endsection

@section('content')
    <div>
         <!-- Page Section -->
         <div class="card p-4 mx-4 ">
            <form action="/profile" method="POST">
                @csrf
                <div class="form-group">
                  <label for="name">Nama</label>
                  <input type="text" class="form-control" name="name" id="name">
                    @error('name')
                        <div class="alert alert-danger">
                            <p>{{ $message }}</p>
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="keterangan">Keterangan</label>
                    <input type="text" class="form-control" name="keterangan" id="keterangan">
                      @error('keterangan')
                        <div class="alert alert-danger">
                            <p>{{ $message }}</p>
                        </div>
                      @enderror
                </div>
                <a href="/profile" class="btn btn-success">Back</a>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
        
         </div>
    </div>
@endsection