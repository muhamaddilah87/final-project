@extends('layouts.master')

@section('title')
    Arsip | Profile
@endsection

@section('judul-page')
    List Profile
@endsection

@section('content')
    <div>
         <!-- Page Section -->
         <div class="card p-4 mx-4 ">
            <div class="d-flex align-items-center justify-content-between mb-4">
                <h5>Profile Information</h5>
                <a href="/profile-create" class="btn btn-success">Create Data</a>
            </div>
           <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead class="text-center">
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Keterangan</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        @foreach ($profileList as $key=>$value)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$value->name}}</td>
                            <td>{{$value->keterangan}}</td>
                            <td class="d-flex justify-content-center">
                                <a href="/profile/{{$value->id}}" class="btn btn-info mx-1">Detail</a>
                                <a href="/profile/{{$value->id}}/edit" class="btn btn-success mx-1">Edit</a>
                                <form action="/profile/{{$value->id}}" method="post" class="mx-1" style="display: inline-block">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
           </div>
         </div>
    </div>
@endsection