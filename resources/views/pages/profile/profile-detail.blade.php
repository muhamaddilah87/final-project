@extends('layouts.master')

@section('title')
    Arsip | Profile
@endsection

@section('judul-page')
    List Profile
@endsection

@section('content')
    <div>
         <!-- Page Section -->
         <div class="card p-4 mx-4 ">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                   
                    <tr>
                        <th>Nama</th>
                        <td>{{$profileData->name}}</td>
                    </tr>
                    <tr>
                        <th>Keterangan</th>
                        <td>{{$profileData->keterangan}}</td>
                    </tr>
                </thead>
            </table>
            <div>
                <a href="/profile" class="btn btn-success">Back</a>
            </div>        
         </div>
    </div>
@endsection