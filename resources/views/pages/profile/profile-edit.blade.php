@extends('layouts.master')

@section('title')
    Arsip | Profile
@endsection

@section('judul-page')
    Edit Profile
@endsection

@section('content')
    <div>
         <!-- Page Section -->
         <div class="card p-4 mx-4 ">
            <form action="/profile/{{$profile->id}}" method="POST">
                @csrf
                @method ('PUT')
                <div class="form-group">
                  <label for="name">Nama</label>
                  <input type="text" value="{{$profile->name}}" class="form-control" name="name" id="name">
                    @error('name')
                        <div class="alert alert-danger">
                            <p>{{ $message }}</p>
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="keterangan">Keterangan</label>
                    <input type="text" value="{{$profile->keterangan}}" class="form-control" name="keterangan" id="keterangan">
                      @error('keterangan')
                        <div class="alert alert-danger">
                            <p>{{ $message }}</p>
                        </div>
                      @enderror
                </div>
                <a href="/profile" class="btn btn-success">Back</a>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
        
         </div>
    </div>
@endsection