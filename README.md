# Repo Final Project Kelompok 6

## Anggota Project Kelompok 6

-   Muhamad Dilah
-   Amalia Bilqis
-   Eka Kamila Dewi

## Tema Project

-   Sistem Arsip Sederhana

## ERD

![ERD.png](ERD.png?raw=true)

## Cara Install

-   Clone Repo Final Project nya Dulu

```bash
git clone https://gitlab.com/muhamaddilah87/final-project.git
```

-   masuk ke direktori repo

-   jalankan perintah composer install untuk mendownload direktori vendor . Note : jika terjadi error coba perintah `composer update` untuk menggantikan perintah composer install

```bash
composer install
```

-   buat file .env lalu isi file tersebut dengan seluruh isi dari file .env.example (copy isi dari .env.example lalu paste di file .env)

-   jalankan perintah php artisan key generate

```bash
php artisan key:generate
```

```bash
php artisan serve
```
